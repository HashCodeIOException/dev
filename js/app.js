//#region UTILS

const fs = require('fs')
var path = require('path');

function readFile(filePath) {
    let lines = fs.readFileSync(filePath).toLocaleString().split("\n")
    let firstLine = lines[0].split(' ').map(s => parseInt(s))
    let rows = lines.slice(1, lines.length - 1).map(s => s.trim());
    return {
        firstLine: firstLine,
        rows: rows
    }
}

function readInputFile(filename) {
    return readFile(path.join(__dirname, "/../input/" + filename + ".in"))
}

function writeFile(filename, firstLine, rows) {
    let lines = [firstLine.join(" ")].concat(rows)
    fs.writeFileSync(path.join(__dirname, "/../output/" + filename + ".out"), lines.join("\n"))
}

//#endregion

//#region INIT

// let FILE_NAME = "a_example"
// let FILE_NAME = "b_lovely_landscapes"
let FILE_NAME = "c_memorable_moments"
// let FILE_NAME = "e_shiny_selfies"

const args = process.argv.slice(2)
if (args[0] != undefined) {
    FILE_NAME = args[0]
}

const {
    firstLine: [N],
    rows: rows
} = readInputFile(FILE_NAME)

const H = 'H'
const V = 'V'

var photos = rows.map((row, i) => {
    let data = row.split(" ")
    return {
        sens: data[0],
        id: i,
        tags: data.slice(2, data[1] + 1),
        used: false
    }
})

photos.sort(function () {
    return .5 - Math.random();
});

var photosH = photos.filter(({
    sens
}) => sens == H)
var photosV = photos.filter(({
    sens
}) => sens == V)

console.time(FILE_NAME)

//#endregion

//#region LOGIQUE

// Fonctions

function scoreTags(tags1, tags2) {
    let commonTagsSize = tags1.filter(t1 => tags2.includes(t1)).length
    return Math.min(tags1.length - commonTagsSize, tags2.length - commonTagsSize, commonTagsSize)
}

function slideTags(slide) {
    return Array.from(new Set(slide.map(photo => photo.tags).flat()))
}


let slides = []

function trouverPremierSlide() {
    return photosH.length > 0 ? [photosH[0]] : [photosV[0], photosV[1]]
}

function trouverMeilleurSlideHSuivant(slidePrecedent) {
    let photosHLocales = photosH.filter(photo => !photo.used)
    let max = Math.floor(slideTags(slidePrecedent).length / 2)-1
    let tagsPrecedent = slideTags(slidePrecedent)
    if (photosHLocales.length < 1) {
        return null
    }
    let bestPhoto = {
        p: photosHLocales[0],
        s: scoreTags(photosHLocales[0].tags, tagsPrecedent)
    }
    try {
        photosHLocales.forEach(photo => {
            let scorePhoto = scoreTags(photo.tags, tagsPrecedent)
            if (bestPhoto.s < scorePhoto) {
                bestPhoto = {
                    p: photo,
                    s: scorePhoto
                }
            }
            if (bestPhoto.s >= max) {
                throw {}
            }
        })
    } catch {}
    return [bestPhoto.p]
}

function trouverMeilleurSlideVSuivant(slidePrecedent) {
    let photosVLocales = photosV.filter(photo => !photo.used)
    let max = Math.floor(slideTags(slidePrecedent).length / 2)-1
    let tagsPrecedent = slideTags(slidePrecedent)
    if (photosVLocales.length < 2) {
        return null
    }
    let bestSlide = {
        p: [photosVLocales[0], photosVLocales[1]],
        s: scoreTags([photosVLocales[0], photosVLocales[1]], tagsPrecedent)
    }
    let listeLimitee = photosVLocales.slice(0, 75)
    try {
        listeLimitee.forEach((photo1, i) => {
            listeLimitee.forEach(photo2 => {
                if (photo1.id === photo2.id){
                    return
                }
                let slide = [photo1, photo2]
                let scoreSlide = scoreTags(slideTags(slide), tagsPrecedent)
                if (bestSlide.s < scoreSlide) {
                    bestSlide = {
                        p: slide,
                        s: scoreSlide
                    }
                }
                if (bestPhoto.s >= max) {
                    throw {}
                }
            })
        })
    } catch {}
    return bestSlide.p
}

function trouverSlideSuivant(slidePrecedent) {
    let slideHSuivant = trouverMeilleurSlideHSuivant(slidePrecedent)
    let slideVSuivant = trouverMeilleurSlideVSuivant(slidePrecedent)
    if (slideHSuivant == null && slideVSuivant == null) {
        return null;
    } else if (slideHSuivant == null) {
        return slideVSuivant
    } else if (slideVSuivant == null) {
        return slideHSuivant
    }
    let tagActuels = slideTags(slidePrecedent)
    return scoreTags(tagActuels, slideTags(slideHSuivant)) < scoreTags(tagActuels, slideTags(slideVSuivant)) ? slideHSuivant : slideVSuivant
}

function ajouterResultat(slide) {
    slide.forEach(photo => photo.used = true)
    slides.push(slide)
}

// Construction du résultat
let slideActuel = trouverPremierSlide()
while (slideActuel != null) {
    ajouterResultat(slideActuel)
    slideActuel = trouverSlideSuivant(slideActuel)
}

//#endregion

//#region FIN

console.timeEnd(FILE_NAME)

writeFile(FILE_NAME, //
    [slides.length], // 
    slides.map(slide => slide.map(photo => photo.id).join(" ")))

// SCORE

let score = 0
slides.forEach((slide, i) => {
    if (i < slides.length - 1) {
        score += scoreTags(slideTags(slide), slideTags(slides[i + 1]))
    }
})

console.log(`${FILE_NAME}: ${score}`)

//#endregion